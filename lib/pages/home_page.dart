import 'package:flutter/material.dart';

  const List<String> itemslist = <String>["Первый", "Второй", "Третий", "Четыертый", "Пятый", "Шестой", "Седьмой"];
  const List<int> numitems = <int>[1,2,3,4,5,6,7];

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ListView'),
        backgroundColor: Colors.cyanAccent,
      ),

      body: ListView.builder(
        itemCount: itemslist.length,
        itemBuilder: (BuildContext context, int index) 
      
      {
        return Container(
          margin: const EdgeInsets.all(16),
          width: double.infinity - 16,
          height: 100,
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 199, 215, 242),
            borderRadius: BorderRadius.circular(16)
          ),
          child: 
          Column( 
            mainAxisAlignment: MainAxisAlignment.spaceBetween, 
            children: <Widget>[
            
             Text('Надпись ${numitems[index]}', style: const TextStyle(fontSize: 22)),
             FloatingActionButton(
              
              onPressed: (null),
              child: Text(
                itemslist[index], 
                style: const TextStyle(
                  color: Colors.white
                  ),
                ),
              ),            
          ],
          ),       

      );
      })
      
    );
  }
}